This repo is for my tutorial on using the fantastic Large Scale Curiosity AI 
(https://github.com/openai/large-scale-curiosity)

It's necessary to clone the original repo, and then replace run.py and wrappers.py with those found in this repo.
Then add watch.py to your repo and you'll be able to teach the ai with the following command:

python run.py --env_kind retro --gamename <some_retro_game> --statename <some_save_state>

